/*
 * This file is generated by jOOQ.
 */
package ru.chsergeig.shoppy.jooq.tables.records;


import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;

import ru.chsergeig.shoppy.jooq.enums.Status;
import ru.chsergeig.shoppy.jooq.tables.Orders;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class OrdersRecord extends UpdatableRecordImpl<OrdersRecord> implements Record3<Integer, String, Status> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>public.orders.id</code>.
     */
    public OrdersRecord setId(Integer value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>public.orders.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>public.orders.info</code>.
     */
    public OrdersRecord setInfo(String value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>public.orders.info</code>.
     */
    public String getInfo() {
        return (String) get(1);
    }

    /**
     * Setter for <code>public.orders.status</code>.
     */
    public OrdersRecord setStatus(Status value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>public.orders.status</code>.
     */
    public Status getStatus() {
        return (Status) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row3<Integer, String, Status> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    @Override
    public Row3<Integer, String, Status> valuesRow() {
        return (Row3) super.valuesRow();
    }

    @Override
    public Field<Integer> field1() {
        return Orders.ORDERS.ID;
    }

    @Override
    public Field<String> field2() {
        return Orders.ORDERS.INFO;
    }

    @Override
    public Field<Status> field3() {
        return Orders.ORDERS.STATUS;
    }

    @Override
    public Integer component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getInfo();
    }

    @Override
    public Status component3() {
        return getStatus();
    }

    @Override
    public Integer value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getInfo();
    }

    @Override
    public Status value3() {
        return getStatus();
    }

    @Override
    public OrdersRecord value1(Integer value) {
        setId(value);
        return this;
    }

    @Override
    public OrdersRecord value2(String value) {
        setInfo(value);
        return this;
    }

    @Override
    public OrdersRecord value3(Status value) {
        setStatus(value);
        return this;
    }

    @Override
    public OrdersRecord values(Integer value1, String value2, Status value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached OrdersRecord
     */
    public OrdersRecord() {
        super(Orders.ORDERS);
    }

    /**
     * Create a detached, initialised OrdersRecord
     */
    public OrdersRecord(Integer id, String info, Status status) {
        super(Orders.ORDERS);

        setId(id);
        setInfo(info);
        setStatus(status);
    }

    /**
     * Create a detached, initialised OrdersRecord
     */
    public OrdersRecord(ru.chsergeig.shoppy.jooq.tables.pojos.Orders value) {
        super(Orders.ORDERS);

        if (value != null) {
            setId(value.getId());
            setInfo(value.getInfo());
            setStatus(value.getStatus());
        }
    }
}
