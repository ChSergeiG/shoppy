/*
 * This file is generated by jOOQ.
 */
package ru.chsergeig.shoppy.jooq.tables.pojos;


import java.io.Serializable;

import ru.chsergeig.shoppy.jooq.enums.Status;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String  info;
    private Status  status;

    public Orders() {}

    public Orders(Orders value) {
        this.id = value.id;
        this.info = value.info;
        this.status = value.status;
    }

    public Orders(
        Integer id,
        String  info,
        Status  status
    ) {
        this.id = id;
        this.info = info;
        this.status = status;
    }

    /**
     * Getter for <code>public.orders.id</code>.
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * Setter for <code>public.orders.id</code>.
     */
    public Orders setId(Integer id) {
        this.id = id;
        return this;
    }

    /**
     * Getter for <code>public.orders.info</code>.
     */
    public String getInfo() {
        return this.info;
    }

    /**
     * Setter for <code>public.orders.info</code>.
     */
    public Orders setInfo(String info) {
        this.info = info;
        return this;
    }

    /**
     * Getter for <code>public.orders.status</code>.
     */
    public Status getStatus() {
        return this.status;
    }

    /**
     * Setter for <code>public.orders.status</code>.
     */
    public Orders setStatus(Status status) {
        this.status = status;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Orders (");

        sb.append(id);
        sb.append(", ").append(info);
        sb.append(", ").append(status);

        sb.append(")");
        return sb.toString();
    }
}
