package ru.chsergeig.shoppy.mapping;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import ru.chsergeig.shoppy.dto.AccountDto;
import ru.chsergeig.shoppy.jooq.tables.pojos.Accounts;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AccountMapper {

    AccountDto map(Accounts pojo);

    Accounts map(AccountDto dto);

}
